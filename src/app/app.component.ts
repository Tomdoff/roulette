import { Component } from '@angular/core';
import { ChipStack } from './chip-stack';
import { Wheel } from './wheel';
import { Chip } from './chip';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  chips: ChipStack[];
  Wheel: Wheel;

  constructor() {
    this.chips = new Array();
  }

  /**
   * TODO
   * creates a stack of chips on the board
   */
  createChipStack(chip: Chip) {
    const chipStack = new ChipStack();
    chipStack.location = chip.location;
    chipStack.numberOfChips = 1;
    chip.values.forEach((value) => {
      console.log(value);
      chipStack.pockets.push(value);
    });
    this.chips.push(chipStack);
    console.log('creating a chip stack');
  }
}

