export class ChipStack {
  // the number of chips placed
  numberOfChips: number;
  // the squares that the chips are on
  pockets: string[];
  // where the chipStack should be placed on the screen
  location: string;

  constructor() {
    this.pockets = new Array();
  }

}
